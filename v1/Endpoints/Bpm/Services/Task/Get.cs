﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Bpm.Services.Task
{
    /// <summary>
    /// Retrieve Task Information
    /// </summary>
    public class Get : Gale.REST.Http.HttpReadActionResult<String>
    {
        private int _limit;
        private int _offset;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <param name="token"></param>
        public Get(int limit, int offset, String token)
            : base(token)
        {
            this._limit = limit;
            this._offset = offset;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_BPM_OBT_Tareas"))
            {
                svc.Parameters.Add("ENTI_Token", this.Model);
                svc.Parameters.Add("Limit", _limit);
                svc.Parameters.Add("Offset", _offset);

                var pagination = this.ExecuteQuery(svc).GetModel<Models.Pagination>().FirstOrDefault();
                var tasks = this.ExecuteQuery(svc).GetModel<Models.Task>(1);

                pagination.items = tasks;

                //Send Response
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                       pagination,
                       System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                return System.Threading.Tasks.Task.FromResult(response);
            }
            //------------------------------------------------------------------------------------------------------
        }
    }
}