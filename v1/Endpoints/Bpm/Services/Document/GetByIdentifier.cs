﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Bpm.Services.Document
{
    /// <summary>
    /// Retrieves Document Information
    /// </summary>
    public class GetByIdentifier : Gale.REST.Http.HttpReadActionResult<string>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="model">Model</param>
        public GetByIdentifier(string model) : base(model) { }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_BPM_OBT_Documento"))
            {
                svc.Parameters.Add("DOCU_Identificador", this.Model);
                var document = this.ExecuteQuery(svc).GetModel<Models.VT_Document>().FirstOrDefault();

                //Send Response
                HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        document,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                return System.Threading.Tasks.Task.FromResult(response);
            }
            //------------------------------------------------------------------------------------------------------
        }
    }
}