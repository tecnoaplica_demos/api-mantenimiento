﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Bpm.Services.Document
{
    /// <summary>
    /// Execute a State Change from the specific document (BPM)
    /// </summary>
    public class Transition : Gale.REST.Http.HttpUpdateActionResult<Models.TransitionData>
    {
        string _action = "";
        string _executor = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token">Document Token</param>
        /// <param name="action">Action Identifier</param>
        /// <param name="data">Transition Data</param>
        public Transition(String executor, string token, string action, Models.TransitionData data)
            : base(token, data)
        {
            this._action = action;
            this._executor = executor;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="token">Document Token</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(string token, System.Threading.CancellationToken cancellationToken)
        {

            //Execute the Document Action
            try
            {
                var document = new Functions.Document(this.Connection, token);
                var recipients = document.Transition(
                    _executor,
                    _action,
                    this.Model
                );
            }
            catch (System.Exception ex)
            {
                var _ex = ex;

                if (ex.InnerException != null)
                {
                    _ex = ex.InnerException;
                }

                throw new Gale.Exception.RestException(System.Net.HttpStatusCode.BadRequest, _ex.Message, null);
            }

            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.PartialContent);
            return System.Threading.Tasks.Task.FromResult(response);
        }
    }
}