﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Endpoints.Bpm.Functions
{
    /// <summary>
    /// BPM - Document Function's
    /// </summary>
    public class Document
    {
        private Gale.Db.IDataActions _connection;
        private string _token;

        /// <summary>
        /// Database Connection
        /// </summary>
        private Gale.Db.IDataActions Db
        {
            get
            {
                return _connection;
            }
        }

        /// <summary>
        /// Document Token
        /// </summary>
        private string Token
        {
            get
            {
                return _token;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connection">Db Connection</param>
        /// <param name="token">Document Token</param>
        public Document(Gale.Db.IDataActions connection, string token)
        {
            _connection = connection;
            _token = token;
        }

        /// <summary>
        /// Execute a State Change from the specific document (BPM) according of his "state machine workflow"
        /// </summary>
        public List<Models.NotificationRecipient> Transition(string executor, string action, Models.TransitionData data)
        {
            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_BPM_ACT_Transicion"))
            {

                svc.Parameters.Add("DOCU_Token", Token);
                svc.Parameters.Add("ENTI_Token", executor);
                svc.Parameters.Add("TRAN_Identificador", action);
                svc.Parameters.Add("BITA_Observacion", data.observation);
                svc.Parameters.Add("ListarDestinatarios", 1);

                Gale.Db.EntityRepository repo = Db.ExecuteQuery(svc);

                //Retrieve the notification list to send email's??
                return repo.GetModel<Models.NotificationRecipient>();
            }
            //------------------------------------------------------------------------------------------------------
        }
    }
}