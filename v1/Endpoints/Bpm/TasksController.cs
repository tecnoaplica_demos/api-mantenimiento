﻿using System;
using System.Web.Http;

namespace API.Endpoints.Bpm
{
    /// <summary>
    /// Task API
    /// </summary>
    [Gale.Security.Oauth.Jwt.Authorize]
    public class InboxController : Gale.REST.RestController
    {
        /// <summary>
        /// Retrieve User Pending Task's
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("Tasks")]
        public IHttpActionResult Get(int limit=10, int offset = 0)
        {
            return new Services.Task.Get(limit, offset, this.User.PrimarySid());
        }

    }
}