﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Bpm
{
    /// <summary>
    /// Document API
    /// </summary>
    [Gale.Security.Oauth.Jwt.Authorize]
    public class DocumentsController : Gale.REST.RestController
    {

        #region DOCUMENT'S
        /// <summary>
        /// List all document's
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.OK)]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.VT_Document>(this.Request);
        }

        /// <summary>
        /// Retrieve Document Information Filtered by Type
        /// </summary>
        /// <param name="type">Document type to filter</param>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("/{type}")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.OK)]
        public IHttpActionResult GetByType(String type)
        {
            //---------------------------------------------------------------------------
            // Setting Static Values
            var config = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            config.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "type_identifier",
                operatorAlias = "eq",
                value = type
            });
            //---------------------------------------------------------------------------

            return new Gale.REST.Http.HttpQueryableActionResult<Models.VT_Document>(this.Request, config);
        }

        /// <summary>
        /// Retrieve Document Information
        /// </summary>
        /// <param name="document">Document Token</param>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("/{document:Guid}")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.OK)]
        public IHttpActionResult Get(Guid document)
        {
            return new Services.Document.Get(document);
        }


        /// <summary>
        /// Execute a State Change from the specific document (BPM) according of his "state machine workflow"
        /// </summary>
        /// <param name="document">Document Token</param>
        /// <param name="actionName">Action Name</param>
        /// <param name="transition">Observation</param>
        /// <returns></returns>
        [HttpPut]
        [HierarchicalRoute("/{document:Guid}/Transition/{actionName}")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.OK)]
        public IHttpActionResult Get(String document, String actionName, Models.TransitionData transition)
        {
            String user = this.User.PrimarySid();

            return new Services.Document.Transition(user, document, actionName, transition);
        }

      
        #endregion

        #region HISTORY
        /// <summary>
        /// Retrieve Document History Information
        /// </summary>
        /// <param name="document">Document Token</param>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("/{document:Guid}/History")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(System.Net.HttpStatusCode.OK)]
        public IHttpActionResult GetHistory(String document)
        {
            return new Services.Document.History(document);
        }
        #endregion
    }
}