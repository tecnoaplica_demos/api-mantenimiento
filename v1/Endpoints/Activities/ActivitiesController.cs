﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Activities
{
    /// <summary>
    /// Activities Controller
    /// </summary>
    public class ActivitiesController : Gale.REST.RestController
    {

        /// <summary>
        /// Retrieve Period Resume
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("{workOrder:Guid}")]
        public IHttpActionResult Get(String workOrder)
        {
            var user = this.User.PrimarySid();

            return new Services.Get(user, workOrder);
        }

        /// <summary>
        /// Retrieve Report
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("{workOrder:Guid}/Report")]
        public IHttpActionResult GetReport(String workOrder)
        {
            var user = this.User.PrimarySid();

            return new Services.Report.Get(user, workOrder);
        }

       
        /// <summary>
        /// Save
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("{workOrder:Guid}")]
        public IHttpActionResult Post(String workOrder, [FromBody]Models.CloseWorkOrder data)
        {
            var user = this.User.PrimarySid();

            return new Services.Close(user, workOrder, data);
        }


        /// <summary>
        /// Retrieve Period Resume
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("Resume")]
        public IHttpActionResult GetResume(String date = null)
        {
            var user = this.User.PrimarySid();

            DateTime _date;

            DateTime.TryParse(date, out _date);
            if (_date == DateTime.MinValue)
            {
                _date = System.DateTime.Now;
            }

            return new Services.GetCurrent(user, _date);
        }


        /// <summary>
        /// Retrieve 
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("Pendings")]
        public IHttpActionResult GetPendings(String date = null)
        {
            var user = this.User.PrimarySid();

            DateTime _date;

            DateTime.TryParse(date, out _date);
            if (_date == DateTime.MinValue)
            {
                _date = System.DateTime.Now;
            }

            return new Services.GetPendings(user, _date);
        }



        [HttpPut]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("{activity:Guid}/Activate")]
        public IHttpActionResult StartActivity(String activity)
        {
            var user = this.User.PrimarySid();

            return new Services.Activate(user, activity);
        }


        #region --> GPS
        /// <summary>
        /// Update the current location for the user
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.Created)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("Gps/Track")]
        [HttpPost]
        public IHttpActionResult UpdateGps([FromBody]Models.GpsPosition position)
        {
            var user = this.User.PrimarySid();
            return new Services.Gps.UpdatePosition(user, position);
        }
        #endregion

        #region --> COMPONENTS

        /// <summary>
        /// Retrieve Period Resume
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize()]
        [HierarchicalRoute("Components/{component:Guid}")]
        public IHttpActionResult Get(String component, String date = null)
        {
            var user = this.User.PrimarySid();

            DateTime _date;

            DateTime.TryParse(date, out _date);
            if (_date == DateTime.MinValue)
            {
                _date = System.DateTime.Now;
            }

            return new Services.Component.GetCurrent(user, component, _date);
        }
        #endregion

    }
}