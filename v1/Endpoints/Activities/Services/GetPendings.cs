﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services
{
    /// <summary>
    /// Get Current Activities
    /// </summary>
    public class GetPendings : Gale.REST.Http.HttpReadActionResult<String>
    {
        DateTime _date;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user"></param>
        public GetPendings(String user, DateTime date)
            : base(user)
        {
            _date = date;
        }


        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_OBT_OrdenesTrabajoPendientes]"))
            {
                svc.Parameters.Add("ENTI_Token", this.Model);
                svc.Parameters.Add("Fecha", _date.Date);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);
                List<Models.OT> workOrders = rep.GetModel<Models.OT>();
              
                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        workOrders,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}