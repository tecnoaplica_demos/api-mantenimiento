﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services
{
    /// <summary>
    /// Get Current Activities
    /// </summary>
    public class GetCurrent : Gale.REST.Http.HttpReadActionResult<String>
    {
        DateTime _date;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user"></param>
        public GetCurrent(String user, DateTime date)
            : base(user)
        {
            _date = date;
        }


        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_OBT_Escritorio]"))
            {
                svc.Parameters.Add("ENTI_Token", this.Model);
                svc.Parameters.Add("Fecha", _date.Date);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.Resume resume = rep.GetModel<Models.Resume>().FirstOrDefault();
                List<Models.ComponentResume> components = rep.GetModel<Models.ComponentResume>(1);

                //--------------------------------------------------------------------
                // ADD EACH SEGMENT :P (TODO: CHANGE LO RESOURCES THE LABEL)
                List<Models.ResumeValue> resumeValues = new List<Models.ResumeValue>();
                resumeValues.Add(new Models.ResumeValue
                {
                    label = "Pendientes",
                    value = resume.pendings,
                    color = "#FFEA00",
                    identifier = "PEND"
                });
                resumeValues.Add(new Models.ResumeValue
                {
                    label = "En Proceso",
                    value = resume.InProccess,
                    color = "#2196F3",
                    identifier = "ENPR"
                });
                resumeValues.Add(new Models.ResumeValue
                {
                    label = "Realizadas",
                    value = resume.realized,
                    color = "#64DD17",
                    identifier = "REAL"
                });


                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        new
                        {
                            statistics = resumeValues,
                            components = components
                        },
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}