﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services.Gps
{
    public class UpdatePosition : Gale.REST.Http.HttpUpdateActionResult<Models.GpsPosition>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="position"></param>
        public UpdatePosition(String token, Models.GpsPosition position) : base(token, position) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(string token, System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_INS_CoordenadaUsuario]"))
            {
                svc.Parameters.Add("ENTI_Token", token);
                svc.Parameters.Add("CORD_Fecha", DateTime.Now);

                //Add Table
                svc.FromModel<Models.GpsPosition>(this.Model);

                this.ExecuteAction(svc);

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.Created);

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}