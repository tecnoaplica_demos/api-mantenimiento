﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services.Report
{
    /// <summary>
    /// Get Details 
    /// </summary>
    public class Get : Gale.REST.Http.HttpReadActionResult<String>
    {
        String _user;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user"></param>
        public Get(String user, String workOrder)
            : base(workOrder)
        {
            _user = user;
        }


        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_OBT_InformeOrdenTrabajo]"))
            {
                svc.Parameters.Add("ENTI_Token",_user);
                svc.Parameters.Add("OTRA_Token", this.Model);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.Report report = rep.GetModel<Models.Report>().FirstOrDefault();
                List<Models.ActivityReport> activities = rep.GetModel<Models.ActivityReport>(1);
                List<Models.File> files = rep.GetModel<Models.File>(2);

                report.activities = activities;
                report.files = files;

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        report,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}