﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services
{
    /// <summary>
    /// Get Details 
    /// </summary>
    public class Get : Gale.REST.Http.HttpReadActionResult<String>
    {
        String _user;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user"></param>
        public Get(String user, String workOrder)
            : base(workOrder)
        {
            _user = user;
        }


        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_OBT_OrdenTrabajo]"))
            {
                svc.Parameters.Add("ENTI_Token",_user);
                svc.Parameters.Add("OTRA_Token", this.Model);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.WorkOrderDetails resume = rep.GetModel<Models.WorkOrderDetails>().FirstOrDefault();
                List<Models.Activity> activities = rep.GetModel<Models.Activity>(1);

                resume.activities = activities;

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        resume,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}