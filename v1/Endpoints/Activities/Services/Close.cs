﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Activities.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class Close : Gale.REST.Http.HttpUpdateActionResult<String>
    {
        Models.CloseWorkOrder _data;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="workOrder"></param>
        /// <param name="data"></param>
        public Close(String user, String workOrder, Models.CloseWorkOrder data)
            : base(workOrder, user)
        {
            _data = data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(string token, System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_MNT_INS_InformeOrdenTrabajo]"))
            {
                svc.Parameters.Add("ENTI_Token", this.Model);
                svc.Parameters.Add("OTRA_Token", token);

                if (_data.image.Length > 0)
                {
                    byte[] image = Convert.FromBase64String(_data.image);
                    svc.Parameters.Add("ARCH_Binario", image);
                    svc.Parameters.Add("ARCH_Tamano", image.Length);
                }
                svc.Parameters.Add("INFO_Observacion", _data.observations);
                svc.Parameters.Add("Actividades", String.Join(",", _data.selecteds.ToArray()));

                this.ExecuteAction(svc);

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.Created);

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}