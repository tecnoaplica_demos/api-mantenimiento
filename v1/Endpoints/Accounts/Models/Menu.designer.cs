﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Endpoints.Accounts.Models
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="AX_Inspecciones")]
	public partial class MenuDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertTB_MAE_ItemMenu(TB_MAE_ItemMenu instance);
    partial void UpdateTB_MAE_ItemMenu(TB_MAE_ItemMenu instance);
    partial void DeleteTB_MAE_ItemMenu(TB_MAE_ItemMenu instance);
    partial void InsertTB_MAE_CategoriaMenu(TB_MAE_CategoriaMenu instance);
    partial void UpdateTB_MAE_CategoriaMenu(TB_MAE_CategoriaMenu instance);
    partial void DeleteTB_MAE_CategoriaMenu(TB_MAE_CategoriaMenu instance);
    #endregion
		
		public MenuDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MenuDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MenuDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public MenuDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<TB_MAE_ItemMenu> TB_MAE_ItemMenus
		{
			get
			{
				return this.GetTable<TB_MAE_ItemMenu>();
			}
		}
		
		public System.Data.Linq.Table<TB_MAE_CategoriaMenu> TB_MAE_CategoriaMenus
		{
			get
			{
				return this.GetTable<TB_MAE_CategoriaMenu>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_MAE_ItemMenu")]
	public partial class TB_MAE_ItemMenu : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _MITE_Codigo;
		
		private int _MITE_MCAT_Codigo;
		
		private string _MITE_Nombre;
		
		private string _MITE_Url;
		
		private string _MITE_Icono;
		
		private System.Nullable<int> _MITE_Ordinal;
		
		private EntityRef<TB_MAE_CategoriaMenu> _TB_MAE_CategoriaMenu;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnMITE_CodigoChanging(int value);
    partial void OnMITE_CodigoChanged();
    partial void OnMITE_MCAT_CodigoChanging(int value);
    partial void OnMITE_MCAT_CodigoChanged();
    partial void OnMITE_NombreChanging(string value);
    partial void OnMITE_NombreChanged();
    partial void OnMITE_UrlChanging(string value);
    partial void OnMITE_UrlChanged();
    partial void OnMITE_IconoChanging(string value);
    partial void OnMITE_IconoChanged();
    partial void OnMITE_OrdinalChanging(System.Nullable<int> value);
    partial void OnMITE_OrdinalChanged();
    #endregion
		
		public TB_MAE_ItemMenu()
		{
			this._TB_MAE_CategoriaMenu = default(EntityRef<TB_MAE_CategoriaMenu>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_Codigo", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int MITE_Codigo
		{
			get
			{
				return this._MITE_Codigo;
			}
			set
			{
				if ((this._MITE_Codigo != value))
				{
					this.OnMITE_CodigoChanging(value);
					this.SendPropertyChanging();
					this._MITE_Codigo = value;
					this.SendPropertyChanged("MITE_Codigo");
					this.OnMITE_CodigoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_MCAT_Codigo", DbType="Int NOT NULL")]
		public int MITE_MCAT_Codigo
		{
			get
			{
				return this._MITE_MCAT_Codigo;
			}
			set
			{
				if ((this._MITE_MCAT_Codigo != value))
				{
					if (this._TB_MAE_CategoriaMenu.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnMITE_MCAT_CodigoChanging(value);
					this.SendPropertyChanging();
					this._MITE_MCAT_Codigo = value;
					this.SendPropertyChanged("MITE_MCAT_Codigo");
					this.OnMITE_MCAT_CodigoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_Nombre", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string MITE_Nombre
		{
			get
			{
				return this._MITE_Nombre;
			}
			set
			{
				if ((this._MITE_Nombre != value))
				{
					this.OnMITE_NombreChanging(value);
					this.SendPropertyChanging();
					this._MITE_Nombre = value;
					this.SendPropertyChanged("MITE_Nombre");
					this.OnMITE_NombreChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_Url", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string MITE_Url
		{
			get
			{
				return this._MITE_Url;
			}
			set
			{
				if ((this._MITE_Url != value))
				{
					this.OnMITE_UrlChanging(value);
					this.SendPropertyChanging();
					this._MITE_Url = value;
					this.SendPropertyChanged("MITE_Url");
					this.OnMITE_UrlChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_Icono", DbType="VarChar(100) NOT NULL", CanBeNull=false)]
		public string MITE_Icono
		{
			get
			{
				return this._MITE_Icono;
			}
			set
			{
				if ((this._MITE_Icono != value))
				{
					this.OnMITE_IconoChanging(value);
					this.SendPropertyChanging();
					this._MITE_Icono = value;
					this.SendPropertyChanged("MITE_Icono");
					this.OnMITE_IconoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MITE_Ordinal", DbType="Int")]
		public System.Nullable<int> MITE_Ordinal
		{
			get
			{
				return this._MITE_Ordinal;
			}
			set
			{
				if ((this._MITE_Ordinal != value))
				{
					this.OnMITE_OrdinalChanging(value);
					this.SendPropertyChanging();
					this._MITE_Ordinal = value;
					this.SendPropertyChanged("MITE_Ordinal");
					this.OnMITE_OrdinalChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_MAE_CategoriaMenu_TB_MAE_ItemMenu", Storage="_TB_MAE_CategoriaMenu", ThisKey="MITE_MCAT_Codigo", OtherKey="MCAT_Codigo", IsForeignKey=true)]
		public TB_MAE_CategoriaMenu TB_MAE_CategoriaMenu
		{
			get
			{
				return this._TB_MAE_CategoriaMenu.Entity;
			}
			set
			{
				TB_MAE_CategoriaMenu previousValue = this._TB_MAE_CategoriaMenu.Entity;
				if (((previousValue != value) 
							|| (this._TB_MAE_CategoriaMenu.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._TB_MAE_CategoriaMenu.Entity = null;
						previousValue.TB_MAE_ItemMenus.Remove(this);
					}
					this._TB_MAE_CategoriaMenu.Entity = value;
					if ((value != null))
					{
						value.TB_MAE_ItemMenus.Add(this);
						this._MITE_MCAT_Codigo = value.MCAT_Codigo;
					}
					else
					{
						this._MITE_MCAT_Codigo = default(int);
					}
					this.SendPropertyChanged("TB_MAE_CategoriaMenu");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_MAE_CategoriaMenu")]
	public partial class TB_MAE_CategoriaMenu : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _MCAT_Codigo;
		
		private string _MCAT_Nombre;
		
		private System.Nullable<System.Guid> _MCAT_Token;
		
		private EntitySet<TB_MAE_ItemMenu> _TB_MAE_ItemMenus;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnMCAT_CodigoChanging(int value);
    partial void OnMCAT_CodigoChanged();
    partial void OnMCAT_NombreChanging(string value);
    partial void OnMCAT_NombreChanged();
    partial void OnMCAT_TokenChanging(System.Nullable<System.Guid> value);
    partial void OnMCAT_TokenChanged();
    #endregion
		
		public TB_MAE_CategoriaMenu()
		{
			this._TB_MAE_ItemMenus = new EntitySet<TB_MAE_ItemMenu>(new Action<TB_MAE_ItemMenu>(this.attach_TB_MAE_ItemMenus), new Action<TB_MAE_ItemMenu>(this.detach_TB_MAE_ItemMenus));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MCAT_Codigo", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int MCAT_Codigo
		{
			get
			{
				return this._MCAT_Codigo;
			}
			set
			{
				if ((this._MCAT_Codigo != value))
				{
					this.OnMCAT_CodigoChanging(value);
					this.SendPropertyChanging();
					this._MCAT_Codigo = value;
					this.SendPropertyChanged("MCAT_Codigo");
					this.OnMCAT_CodigoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MCAT_Nombre", DbType="VarChar(50)")]
		public string MCAT_Nombre
		{
			get
			{
				return this._MCAT_Nombre;
			}
			set
			{
				if ((this._MCAT_Nombre != value))
				{
					this.OnMCAT_NombreChanging(value);
					this.SendPropertyChanging();
					this._MCAT_Nombre = value;
					this.SendPropertyChanged("MCAT_Nombre");
					this.OnMCAT_NombreChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_MCAT_Token", DbType="UniqueIdentifier")]
		public System.Nullable<System.Guid> MCAT_Token
		{
			get
			{
				return this._MCAT_Token;
			}
			set
			{
				if ((this._MCAT_Token != value))
				{
					this.OnMCAT_TokenChanging(value);
					this.SendPropertyChanging();
					this._MCAT_Token = value;
					this.SendPropertyChanged("MCAT_Token");
					this.OnMCAT_TokenChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="TB_MAE_CategoriaMenu_TB_MAE_ItemMenu", Storage="_TB_MAE_ItemMenus", ThisKey="MCAT_Codigo", OtherKey="MITE_MCAT_Codigo")]
		public EntitySet<TB_MAE_ItemMenu> TB_MAE_ItemMenus
		{
			get
			{
				return this._TB_MAE_ItemMenus;
			}
			set
			{
				this._TB_MAE_ItemMenus.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_TB_MAE_ItemMenus(TB_MAE_ItemMenu entity)
		{
			this.SendPropertyChanging();
			entity.TB_MAE_CategoriaMenu = this;
		}
		
		private void detach_TB_MAE_ItemMenus(TB_MAE_ItemMenu entity)
		{
			this.SendPropertyChanging();
			entity.TB_MAE_CategoriaMenu = null;
		}
	}
}
#pragma warning restore 1591
