﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;

namespace API.Endpoints.Accounts.Services
{
    /// <summary>
    /// Register User in DB (By Register Email)
    /// </summary>
    public class Register : Gale.REST.Http.HttpUpdateActionResult<Models.Register>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token">User Token</param>
        /// <param name="user">Target Model</param>
        public Register(string token, Models.Register user) : base(token, user) {
            user.token = token;
        }

        /// <summary>
        ///  Update User
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(string token, System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => Model == null, "BODY_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(Model.fullname), "NAME_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(Model.email), "EMAIL_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_MAE_ACT_RegistrarUsuario"))
            {
                svc.Parameters.Add("ENTI_Token", HttpContext.Current.User.PrimarySid());
                svc.Parameters.Add("ENTI_Identificador", Model.fullname);
                svc.Parameters.Add("USUA_NombreCompleto", Model.fullname);
                svc.Parameters.Add("USUA_Token", Model.token);
                svc.Parameters.Add("USUA_Email", Model.email);
                svc.Parameters.Add("USUA_Contrasena", Gale.Security.Cryptography.MD5.GenerateHash(Model.password));

                if (Model.avatar != null && Model.avatar != System.Guid.Empty)
                {
                    svc.Parameters.Add("ARCH_Token", Model.avatar);
                }

                this.ExecuteAction(svc);
            }
            //------------------------------------------------------------------------------------------------------

            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.PartialContent);

            return Task.FromResult(response);
        }
    }
}