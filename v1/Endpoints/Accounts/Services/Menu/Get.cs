﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Accounts.Services.Menu
{
    /// <summary>
    /// Retrieves the Menu from a user
    /// </summary>
    public class Get : Gale.REST.Http.HttpReadActionResult<String>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token">Account Identifier</param>
        public Get(string token) : base(token) { }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_MAE_SEL_Menu"))
            {
                svc.Parameters.Add("USUA_Token", this.Model);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                List<Models.TB_MAE_CategoriaMenu> db_categories = rep.GetModel<Models.TB_MAE_CategoriaMenu>();
                List<Models.TB_MAE_ItemMenu> db_items = rep.GetModel<Models.TB_MAE_ItemMenu>(1);

                //------------------------------------------------------------------------------------------------------
                // GUARD EXCEPTIONS
                Gale.Exception.RestException.Guard(() => db_categories.Count == 0, "USER_DONT_HAVE_MENU", API.Errors.ResourceManager);
                //------------------------------------------------------------------------------------------------------

                var menu = (from t in db_categories
                            select new
                            {
                                name = t.MCAT_Nombre,
                                token = t.MCAT_Token,
                                open = true,
                                items = (from item in db_items
                                         where item.MITE_MCAT_Codigo == t.MCAT_Codigo
                                         orderby item.MITE_Ordinal ascending
                                         select new
                                         {
                                             name = item.MITE_Nombre,
                                             icon = item.MITE_Icono,
                                             url = item.MITE_Url
                                         })
                            });

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        menu,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.JsonFormatter
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}