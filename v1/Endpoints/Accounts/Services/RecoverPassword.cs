﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;
using RazorTemplates.Core;
using System.Net.Mail;

namespace API.Endpoints.Accounts.Services
{
    /// <summary>
    /// Send And Email to the user for recovering Password
    /// </summary>
    public class RecoverPassword : Gale.REST.Http.HttpCreateActionResult<String>
    {
        string _host;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token">User Token</param>
        /// <param name="user">Target Model</param>
        public RecoverPassword(string email, String host) : base(email) {
            _host = host;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string TemplateBody(dynamic model)
        {
            //----------------------------------
            var assembly = this.GetType().Assembly;
            String resourcePath = "API.Endpoints.Accounts.Templates.Mail.RecoverPassword.cshtml";

            using (System.IO.Stream stream = assembly.GetManifestResourceStream(resourcePath))
            {
                //------------------------------------------------------------------------------------------------------
                // GUARD EXCEPTIONS
                Gale.Exception.RestException.Guard(() => stream == null, "TEMPLATE_DONT_EXIST", API.Errors.ResourceManager);
                //------------------------------------------------------------------------------------------------------

                using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
                {
                    var view = Template.Compile(reader.ReadToEnd());
                    return view.Render(model);
                }
            }
            //----------------------------------
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_MAE_OBT_UsuarioParaRecuperarContrasena"))
            {
                svc.Parameters.Add("USUA_Email", this.Model);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.Account account = rep.GetModel<Models.Account>().FirstOrDefault();
                
                //----------------------------------------------------------------------------------------------------
                //Guard Exception's
                Gale.Exception.RestException.Guard(() => account == null, "ACCOUNT_DONT_EXISTS", API.Errors.ResourceManager);
                //----------------------------------------------------------------------------------------------------


                //----------------------------------------------------------------------
                //Send an Activation Email

                // REPLICATES A BASIC TOKEN AND 2 HOURS EXPIRATION
                List<System.Security.Claims.Claim> claims = new List<System.Security.Claims.Claim>();

                claims.Add(new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.Email, account.email));
                claims.Add(new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.PrimarySid, account.token.ToString()));
                claims.Add(new System.Security.Claims.Claim(System.Security.Claims.ClaimTypes.Name, account.fullname));
                claims.Add(new System.Security.Claims.Claim("avatar", account.avatar.ToString()));
                var token = Gale.Security.Oauth.Jwt.Manager.CreateToken(claims, DateTime.Now.AddMinutes(60 * 2));  //2 Horas		

                //Wrap the Message
                MailMessage message = new MailMessage()
                {
                    IsBodyHtml = true,
                    From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["Mail:Account"]),
                    Subject = Templates.Mail.Mail.Recover_Subject,
                    Body = TemplateBody(new
                    {
                        Nombre = account.fullname,
                        Url = String.Format("{0}#/public/accounts/register/confirm/{1}", this._host, token.access_token)
                    })
                };
                message.To.Add(new MailAddress(account.email));
                SmtpClient client = new SmtpClient();
                client.Send(message);
                //----------------------------------------------------------------------
            }

            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.Created);
            return Task.FromResult(response);

        }
    }
}